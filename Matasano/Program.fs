﻿open Challenge0108

printfn "Enter a key:"
let mutable key = System.Console.ReadLine()

let mutable value = ""

while key <> "" do

    printfn "Enter a value:"
    value <- System.Console.ReadLine()

    while value <> "" do
        repeatingKeyXor key (System.Text.Encoding.ASCII.GetBytes(value))
        |> Seq.map (fun b -> b |> System.String.Concat)
        |> String.concat System.String.Empty
        |> printfn "%s"

        printfn "Enter a value:"
        value <- System.Console.ReadLine()

    printfn "Enter a key:"
    key <- System.Console.ReadLine()

