﻿module Challenge0108

open System
open System.Security.Cryptography
open System.IO
open System.Text

open NUnit.Framework
open FsUnit

//1 Convert hex to base64 and back.
//
//The string:
//
//  49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d
//
//should produce:
//
//  SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t
//
//Now use this code everywhere for the rest of the exercises. Here's a
//simple rule of thumb:
//
//  Always operate on raw bytes, never on encoded strings. Only use hex
//  and base64 for pretty-printing.

let fromHex s = 
    s
    |> Seq.windowed 2
    |> Seq.mapi (fun i j -> (i,j))
    |> Seq.where (fun (i,j) -> i % 2=0)
    |> Seq.map (fun (_,j) -> Byte.Parse(new System.String(j), Globalization.NumberStyles.AllowHexSpecifier))
    |> Array.ofSeq

let hexToBase64 s =
    s
    |> fromHex
    |> Convert.ToBase64String

[<Test>]
let ``01a convert hex string to base64`` () =
    hexToBase64 "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d" 
        |> should equal "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"

let toHex b =
    b
    |> Array.map (fun (x : byte) -> System.String.Format("{0:X2}", x).ToLower())
    |> String.concat String.Empty

let base64ToHex s =
    s
    |> Convert.FromBase64String
    |> toHex
    
[<Test>]
let ``01b convert base64 string to hex`` () =
    base64ToHex "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"
        |> should equal "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"

//2 Fixed XOR
//
//Write a function that takes two equal-length buffers and produces
//their XOR sum.
//
//The string:
//
// 1c0111001f010100061a024b53535009181c
//
//... after hex decoding, when xor'd against:
//
// 686974207468652062756c6c277320657965
//
//... should produce:
//
// 746865206b696420646f6e277420706c6179

let xor (b1:byte[]) (b2:byte[]) =
        b1
        |> Seq.mapi (fun i b -> b1.[i] ^^^ b2.[i])
        |> Array.ofSeq
        
[<Test>]
let ``02a calculate XOR sum of two equal-length buffers`` () =
    xor (fromHex "1c0111001f010100061a024b53535009181c") (fromHex "686974207468652062756c6c277320657965")
    |> toHex
    |> should equal "746865206b696420646f6e277420706c6179"

//3 Single-character XOR Cipher
//
//The hex encoded string:
//
//      1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736
//
//... has been XOR'd against a single character. Find the key, decrypt
//the message.
//
//Write code to do this for you. How? Devise some method for "scoring" a
//piece of English plaintext. (Character frequency is a good metric.)
//Evaluate each output and choose the one with the best score.
//
//Tune your algorithm until this works.

let xorSingleChar (b:byte[]) c =
    b
    |> Seq.map (fun x -> x ^^^ c)
    |> Array.ofSeq

let bytesToString b =
    b
    |> System.Text.Encoding.ASCII.GetChars
    |> System.String.Concat 

let countOccurrences a b =
    b
    |> Seq.where (fun b' -> a |> Seq.exists ((=) b'))
    |> Seq.length

[<Test>]
let ``03a countOccurrences of 'a' and 'c' in "abcdefbcdefg" should be 3`` () =
    "abcdefbcdefg" |> countOccurrences [ 'a'; 'c' ] |> should equal 3

[<Test>]
let ``03b countOccurrences of 'b' and 'f' in "abcdefbcdefg" should be 4`` () =
    countOccurrences [ 'b'; 'f' ] "abcdefbcdefg" |> should equal 4

let keys = [ ' ' ] @ [ '!' .. 'z' ]
let letterFrequencies = // from http://en.wikipedia.org/wiki/Letter_frequency
    Map.ofList [ 
        ' ',  10.0 // guessing at a frequency of a space. Algorithm doesn't work without counting spaces towards the score.
        'a', 8.167
        'b', 1.492
        'c', 2.782
        'd', 4.253
        'e', 12.702
        'f', 2.228
        'g', 2.015
        'h', 6.094
        'i', 6.966
        'k', 0.772
        'l', 4.025
        'm', 2.406
        'n', 6.749
        'o', 7.507
        'p', 1.929
        'q', 0.095
        'r', 5.987
        's', 6.327
        't', 9.056
        'u', 2.758
        'v', 0.978
        'w', 2.360
        'x', 0.150
        'y', 1.974
        'z', 0.074
    ]

[<Test>]
let ``03c letterFrequencies of space equals 10.0`` () =
    letterFrequencies.[' '] |> should equal 10.0

let getScore (s:string) =
    s.ToLower()
    |> Seq.map (fun c -> if letterFrequencies.ContainsKey(c) then letterFrequencies.[c] else -2.0)
    |> Seq.sum

let breakSingleCharXorCipher bytes =
    keys
    |> Array.ofList
    |> Array.Parallel.map (fun c -> (c, xorSingleChar bytes (byte c) |> bytesToString ) )
    |> Array.Parallel.map (fun (c, d) -> (c, getScore d, d ))
    |> Array.maxBy (fun (c, score, d) -> score)

[<Test>]
let ``03d Find the key and decrypt the message`` () =
    "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736" 
    |> fromHex
    |> breakSingleCharXorCipher 
    |> (fun (c, s, d) -> (c, d)) // don't care about the score for this test as long as it continues to find the right answer.
    |> should equal ('X', "Cooking MC's like a pound of bacon")

//4 Detect single-character XOR
//
//One of the 60-character strings at:
//
//  https://gist.github.com/3132713
//
//has been encrypted by single-character XOR. Find it. (Your code from
//#3 should help.)

let detectSingleCharXor filePath =
    System.IO.File.ReadLines(filePath)
    |> Array.ofSeq
    |> Array.Parallel.mapi (fun i line -> (i, line |> fromHex |> breakSingleCharXorCipher ))
    //|> Array.map (fun l -> printfn "%A" l)
    |> Array.maxBy (fun (i, r) -> r |> (fun (c, score, d ) -> score)) 
    |> (fun (i,(c,s,d)) -> (i,c,s,d))

[<Test>]
let ``04a Detect the single-char XOR line`` () =
    detectSingleCharXor "Challenge04.txt"
    |> (fun (i, c, s, d) -> (i, c, d.Trim())) // ignore score for test
    |> should equal (170, '5', "Now that the party is jumping")

//5 Repeating-key XOR Cipher
//
//Write the code to encrypt the string:
//
//  Burning 'em, if you ain't quick and nimble
//  I go crazy when I hear a cymbal
//
//Under the key "ICE", using repeating-key XOR. It should come out to:
//
//  0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f
//
//Encrypt a bunch of stuff using your repeating-key XOR function. Get a
//feel for it.

// spill and breakBy from http://fssnip.net/1o
let spill (n:int) (s:seq<'a>)  = 
    let en = s.GetEnumerator()
    let pos = ref 0
    let lst = [ while !pos < n && en.MoveNext() do 
                    pos := !pos+1  
                    yield en.Current]
    if lst |> List.isEmpty then None else
        Some((lst, seq { while en.MoveNext() do yield en.Current}))

let breakBy n = Seq.unfold (spill n)

let repeatingKeyXor (key:string) (s:byte[]) =
    let keyLength = key.Length
    let keyBytes = System.Text.Encoding.ASCII.GetBytes(key)
    s
    |> breakBy keyLength
    |> Seq.map (fun b -> xor (b |> Array.ofList) keyBytes)
    |> Seq.collect (fun x -> x)
    |> Array.ofSeq

[<Test>]
let ``05a encrypt input string with "ICE"`` () =
    System.Text.Encoding.ASCII.GetBytes( "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal")
    |> repeatingKeyXor "ICE"
    |> toHex
    |> should equal "0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f"

[<Test>]
let ``05b decrypt string with key "ICE"`` () = // After working part way through challenge 6 I realized that repeatingKeyXor is both encrypt and decrypt with the same key. This test verifies that realization.
    "0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f"
    |> fromHex 
    |> repeatingKeyXor "ICE"
    |> System.Text.Encoding.ASCII.GetChars
    |> System.String.Concat
    |> should equal "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal"

//6 Break repeating-key XOR
//
//The buffer at the following location:
//
// https://gist.github.com/3132752
//
//is base64-encoded repeating-key XOR. Break it.
//
//Here's how:
//
//a. Let KEYSIZE be the guessed length of the key; try values from 2 to
//(say) 40.
//
//b. Write a function to compute the edit distance/Hamming distance
//between two strings. The Hamming distance is just the number of
//differing bits. The distance between:
//
//  this is a test
//
//and:
//
//  wokka wokka!!!
//
//is 37.

// count method modified from http://fssnip.net/Q
type System.Byte with
    member x.count = // count bits set to 1
        let rec go b acc = if b = 0uy then acc else go (b &&& (b-1uy)) (acc+1) //sparse count
        go x 0

let hammingDistance (a:byte[]) (b:byte[]) =
    xor a b
    |> Array.map (fun x -> x.count)
    |> Array.sum

let hammingDistanceOfStrings (a:string) (b:string) = 
    hammingDistance (System.Text.Encoding.ASCII.GetBytes(a)) (System.Text.Encoding.ASCII.GetBytes(b))

[<Test>]
let ``06a hammingDistance between "this is a test" and "wokka wokka!!!" should be 37`` () =
    hammingDistanceOfStrings "this is a test" "wokka wokka!!!" |> should equal 37

//
//c. For each KEYSIZE, take the FIRST KEYSIZE worth of bytes, and the
//SECOND KEYSIZE worth of bytes, and find the edit distance between
//them. Normalize this result by dividing by KEYSIZE.

/// Floating point division given int and int args. from http://fssnip.net/fy
let (./.) x y = 
    (x |> double) / (y |> double)

let normalizedEditDistance keySize (text:byte[]) =
    text
    |> breakBy keySize
    |> Seq.take 2
    |> Array.ofSeq
    |> Array.map Array.ofList
    |> (fun a -> (hammingDistance a.[0] a.[1]) ./. keySize)

[<Test>]
let ``06b normalizedEditDistance "this is a testwokka wokka!!!" keySize 14 should be ~2.64285714`` () =
    System.Text.Encoding.ASCII.GetBytes("this is a testwokka wokka!!!")
    |> normalizedEditDistance 14 
    |> should (equalWithin 0.00000001) 2.64285714

//d. The KEYSIZE with the smallest normalized edit distance is probably
//the key. You could proceed perhaps with the smallest 2-3 KEYSIZE
//values. Or take 4 KEYSIZE blocks instead of 2 and average the
//distances.

let averageEditDistance keySize blocks (text:byte[]) =
    text
    |> breakBy keySize
    |> Seq.take blocks
    |> Seq.windowed 2
    |> Seq.map (fun a -> hammingDistance (Array.ofList a.[0]) (Array.ofList a.[1]))
    |> Seq.averageBy (fun elem -> float elem)
    |> (fun a -> a / (keySize |> float))

let bestKeySizes text = 
    [ 2 .. 40 ]
    |> Array.ofList
    |> Array.map (fun keySize -> (keySize, (averageEditDistance keySize 4 text)))
    |> Array.sortBy (fun (_,d) -> d)
    |> Array.map (fun (k,_) -> k)
    |> Seq.ofArray

[<Test>]
let ``06c bestKeySizes given challenge text`` () =
    System.Convert.FromBase64String(System.IO.File.ReadAllText("Challenge06.txt"))
    |> bestKeySizes 
    |> Seq.take 4
    |> should equal [ 2; 3; 29; 5; ]
 
//e. Now that you probably know the KEYSIZE: break the ciphertext into
//blocks of KEYSIZE length.
//
//f. Now transpose the blocks: make a block that is the first byte of
//every block, and a block that is the second byte of every block, and
//so on.

let nthElementFromEach n (blocks:seq<'a[]>) = 
    blocks
    |> Seq.map (fun b -> b.[n])

let transposeBlocks size blocks =
    [ 0 .. (size - 1) ]
    |> Seq.map (fun n -> nthElementFromEach n blocks |> Array.ofSeq)

[<Test>]
let ``06d transposeBlocks "abcd efgh ijkl mnop qrst uvwx yz" with block size 5 returns "aeimqubfjnrvcgkoswdhlptx      "`` () =
    "abcd efgh ijkl mnop qrst uvwx yz"
    |> breakBy 5
    |> Seq.map Array.ofList
    |> Seq.where (fun b -> (b |> Array.length) = 5) // ignore extra odd sized block at the end to avoid IndexOutOfRangeException 
    |> transposeBlocks 5
    |> Seq.map (fun b -> b |> System.String.Concat)
    |> String.concat System.String.Empty
    |> should equal "aeimqubfjnrvcgkoswdhlptx      "

//g. Solve each block as if it was single-character XOR. You already
//have code to do this.
//
//e. For each block, the single-byte XOR key that produces the best
//looking histogram is the repeating-key XOR key byte for that
//block. Put them together and you have the key.

let breakRepeatingKeyXorCipherForKeySize text keySize =
    text
    |> breakBy keySize
    |> Seq.map Array.ofList
    |> Seq.where (fun b -> (b |> Array.length) = keySize) // ignore extra odd sized block at the end to avoid IndexOutOfRangeException 
    |> transposeBlocks keySize
    |> Seq.map breakSingleCharXorCipher
    |> Seq.map (fun (c,_,_) -> c)
    |> System.String.Concat

let repeatingKeyXorCipherPossibleKeys x text =
    text
    |> bestKeySizes
    |> Seq.take x
    |> Seq.map (breakRepeatingKeyXorCipherForKeySize text)

[<Test>]
let ``06e repeatingKeyXorCipherPossibleKeys for given challenge text with best key sizes`` () =
    System.Convert.FromBase64String(System.IO.File.ReadAllText("Challenge06.txt"))
    |> repeatingKeyXorCipherPossibleKeys 3
    |> should equal [ "nn"; "nnt"; "Terminator X: Bring the noise" ]

[<Test>]
let ``06f decrypt text with found key`` () =
    System.Convert.FromBase64String(System.IO.File.ReadAllText("Challenge06.txt"))
    |> repeatingKeyXor "Terminator X: Bring the noise"
    |> System.Text.Encoding.ASCII.GetChars
    |> System.String.Concat
    //|> (fun a -> System.IO.File.WriteAllText("Challenge06answer.txt", a))
    |> should equal (System.IO.File.ReadAllText("Challenge06answer.txt"))

//7. AES in ECB Mode
//
//The Base64-encoded content at the following location:
//
//    https://gist.github.com/3132853
//
//Has been encrypted via AES-128 in ECB mode under the key
//
//    "YELLOW SUBMARINE".
//
//(I like "YELLOW SUBMARINE" because it's exactly 16 bytes long).
//
//Decrypt it.
//
//Easiest way:
//
//Use OpenSSL::Cipher and give it AES-128-ECB as the cipher.

let DecryptStringFromBytes_Aes_128_ECB key (cipherText:byte[]) =
    use aesAlg = new AesManaged()
    aesAlg.KeySize <- 128
    aesAlg.Mode <- CipherMode.ECB
    aesAlg.Key <- key
    let decryptor = aesAlg.CreateDecryptor()
    use msDecrypt = new MemoryStream(cipherText)
    use csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read)
    use srDecrypt = new StreamReader(csDecrypt)
    srDecrypt.ReadToEnd()

[<Test>]
let ``07a decrypt challenge text with key YELLOW SUBMARINE`` () =
    Convert.FromBase64String(File.ReadAllText("Challenge07.txt"))
    |> DecryptStringFromBytes_Aes_128_ECB (Encoding.ASCII.GetBytes("YELLOW SUBMARINE"))
    //|> (fun a -> System.IO.File.WriteAllText("Challenge07answer.txt", a))
    |> should equal (File.ReadAllText("Challenge06answer.txt"))

//8. Detecting ECB
//
//At the following URL are a bunch of hex-encoded ciphertexts:
//
//   https://gist.github.com/3132928
//
//One of them is ECB encrypted. Detect it.
//
//Remember that the problem with ECB is that it is stateless and
//deterministic; the same 16 byte plaintext block will always produce
//the same 16 byte ciphertext.


// So, the answer is which line has the smallest hamming distance between 16 byte blocks?

let averageEditDistance' keySize (text:byte[]) =
    text
    |> breakBy keySize
    |> Seq.windowed 2
    |> Seq.map (fun a -> hammingDistance (Array.ofList a.[0]) (Array.ofList a.[1]))
    |> Seq.averageBy (fun elem -> float elem)

let detectECBText text =
    text
    |> Array.Parallel.map (fun t -> (averageEditDistance' 16 t, t))
    |> Array.maxBy (fun (d,_) -> d)
    |> fun (_,t) -> t

[<Test>]
let ``08a detectECBText given challenge input`` () =
    File.ReadAllLines("Challenge08.txt")
    |> Array.map fromHex
    |> detectECBText
    |> toHex
    |> should equal "96b0db74959d0c3ec0819f3d7239d8f4e74c0fab50f8a99130e5d2c80e4917f7632679ef0bfd303ad61fe005fed62afd574a75a014042a4b75eac3948886b2c99cd9bece7cef4fbc86c816b9cf6441040593759adef2ce62fd3bd97dc4d496b02c3a883172610af18ed57322d98b0b7f51776229969e27a06b6445c22651b6aea4dda50761d04ccb3bf1d99e353d11b967f24c08c1eba16beff4a2898ff1e641"
